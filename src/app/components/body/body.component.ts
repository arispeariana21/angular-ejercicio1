import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  lorem = 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Natus error accusantium vel quia rem explicabo reprehenderit aperiam commodi et labore quis, debitis velit. Numquam, illum corrupti! Doloribus exercitationem repellat molestias?';
  constructor() { }

  ngOnInit(): void {
  }

}
